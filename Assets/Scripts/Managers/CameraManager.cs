using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public bool rotating;
    public float rotateSpeed = 2f;
    float timer = 0f;

    void Update()
    {
        if (rotating)
        {
            Vector3 direction = transform.position;

            if (GameManager.Instance.activePlayer != GameManager.Instance.alivePlayers && GameManager.Instance.activePlayer >= 0)
            {
                if (GameManager.Instance.gameState != GameState.dealerTurn)
                {
                    direction = GameManager.Instance.gamePlayers[GameManager.Instance.activePlayer].playerScore.transform.position - transform.position;
                }
            }
            else if (GameManager.Instance.gameState == GameState.dealerTurn)
            {
                direction = GameManager.Instance.DeckManager.dealerDeckPosition.position - transform.position;
            }
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * rotateSpeed);
            timer -= Time.deltaTime;

            if (transform.rotation == targetRotation || timer <= 0)
            {
                rotating = false;
            }

        }
    }


    public void StartRotation()
    {
        rotating = true;
        timer = 2f;
    }

    public void StopRotation()
    {
        rotating = false;
    }
    private void OnDisable()
    {
        StopRotation();
    }
}
