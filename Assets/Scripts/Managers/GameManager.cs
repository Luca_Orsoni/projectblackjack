using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public List<PlayerAI> gamePlayers = new List<PlayerAI>();
    //[HideInInspector]
    public int activePlayer;
    public GameState gameState = GameState.waiting;
    [HideInInspector]
    public bool deckMustBeShuffled = true;
    public float dealSpeedMultiplyer = 1;
    public int alivePlayers;
    bool[] winLose;
    public static event Action<GameState> gameStateChanged;
    public int victories = 0;
    public int defeats = 0;

    //-----------------------------------------------------------------------
    // Managers & UI
    //-----------------------------------------------------------------------
    [SerializeField]
    protected GameSettings gameSettings;
    [SerializeField]
    protected AudioManager audioManager;
    [SerializeField]
    private DeckManager deckManager;
    [SerializeField]
    private PlayerSpawner playerSpawner;
    [SerializeField]
    private UIManager ui;
    [SerializeField]
    private CameraManager cameraManager;

    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------

    #region Properties

    public UIManager UI
    {
        get
        {
            return ui;
        }
    }

    public GameSettings Settings
    {
        get
        {
            return gameSettings;
        }
    }

    public AudioManager AudioManager
    {
        get
        {
            return audioManager;
        }
    }

    public DeckManager DeckManager
    {
        get
        {
            return deckManager;
        }
    }

    public CameraManager CameraManager
    {
        get
        {
            return cameraManager;
        }
    }

    #endregion

    public override void Awake()
    {
        base.Awake();

        gameSettings.playersNumber = PlayerPrefs.GetInt("PlayersNumber", 7);
        gameSettings.aiLevel = PlayerPrefs.GetInt("AILevel");
        playerSpawner.SpawnPlayers(gameSettings.playersNumber);
    }

    private void Start()
    {
        SetInitialGameState();

        deckMustBeShuffled = true;

        alivePlayers = gameSettings.playersNumber;
        activePlayer = alivePlayers;
        deckManager.GenerateCards();
        deckManager.CreateDeck();

        ui.MessagesDisplayer.DisplayMessage("Shuffle the deck and\npress PLAY", 0, 0);
    }

    public void SetInitialGameState()
    {
        gameState = GameState.waiting;
        foreach (var p in gamePlayers)
        {
            p.waitingForACard = true;
            p.cardsValue = 0;
        }
        deckManager.yourCardsValue = 0;
        gameStateChanged?.Invoke(GameState.waiting);
        deckManager.PlaceDeck();
        ui.FlipOrDrawCardsButtonUpdate(true);
        ui.UpdatePDealerUI($"Players cards values: ?");
        ui.UpdateDDealerUI($"Your cards value: ?");

    }

    public void StartFirstTurn()
    {
        ui.MessagesDisplayer.DismissMessage();

        gameState = GameState.firstTurn;
        gameStateChanged?.Invoke(GameState.firstTurn);

        Deal();
    }

    public void RemovePlayer(PlayerAI deadPlayer)
    {
        alivePlayers--;
        if (alivePlayers == 0)
        {
            ui.MessagesDisplayer.DisplayMessage("YOU KILLED THEM ALL!!!\n(go back to main menu\nto start a new game)", 0, 0);
            UI.DeactivateAll();
        }
        else
        {
            gamePlayers.Remove(deadPlayer);
            activePlayer--;
        }
    }

    public async void Deal()
    {
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < gamePlayers.Count; j++)
            {
                gamePlayers[j].speed *= (2 * dealSpeedMultiplyer);
                deckManager.deck[0].PlayDrawCard();
                await Task.Delay((int)(500 / dealSpeedMultiplyer));
                deckManager.GiveCard(gamePlayers[j]);
                await Task.Delay((int)(1000 / dealSpeedMultiplyer));
                gamePlayers[j].waitingForACard = true;
                gamePlayers[j].speed /= (2 * dealSpeedMultiplyer);

                ui.UpdateActivePlayerScore(j, gamePlayers[j].cardsValue);
            }
            deckManager.DrawACard();
            await Task.Delay((int)(1500 / dealSpeedMultiplyer));
        }
        foreach (var p in gamePlayers)
        {
            p.MakeChoice();
        }

        ui.MessagesDisplayer.DisplayMessage("Ready to start the first\nplayer turn", 0, 0);

        ui.nextButton.SetActive(true);
    }

    public void SetActivePlayer()
    {
        int pn = alivePlayers;
        switch (activePlayer)
        {
            case int n when (n + 2 == pn):
                activePlayer++;
                gameState = GameState.playersTurn;
                gameStateChanged?.Invoke(GameState.playersTurn);
                PlayerTurn();
                break;
            case int n when (n == pn):
                activePlayer = 0;
                gameState = GameState.playersTurn;
                gameStateChanged?.Invoke(GameState.playersTurn);
                PlayerTurn();
                break;
            case int n when (n + 1 == pn):
                activePlayer++;
                gameState = GameState.dealerTurn;
                gameStateChanged?.Invoke(GameState.dealerTurn);
                DealerTurn();
                break;
            default:
                activePlayer++;
                gameState = GameState.playersTurn;
                gameStateChanged?.Invoke(GameState.playersTurn);
                PlayerTurn();
                break;
        }

    }

    public void PlayerTurn()
    {
        cameraManager.StartRotation();
        gamePlayers[activePlayer].playerScore.transform.parent.gameObject.SetActive(true);

        gamePlayers[activePlayer].ShowChoice();
    }

    public void ContinuePlayerTurn()
    {
        gamePlayers[activePlayer].MakeChoice();
        gamePlayers[activePlayer].ShowChoice();
    }

    public void DealerTurn()
    {
        gameState = GameState.dealerTurn;

        ui.MessagesDisplayer.DisplayMessage("DealerTurn");
        cameraManager.StartRotation();

        ui.UpdatePDealerUI(PlayersScoresText());
    }

    string PlayersScoresText(bool endGame = false)
    {
        if (endGame)
        {
            string playerScores = "You defeat player: ";
            for (int i = 0; i < winLose.Length; i++)
            {
                if (winLose[i])
                {
                    victories++;
                    playerScores += $"P{gamePlayers[i].PlayerNumber + 1}  ";
                }
            }
            playerScores += "\nYou lose against player: ";
            for (int i = 0; i < winLose.Length; i++)
            {
                if (!winLose[i])
                {
                    defeats++;
                    playerScores += $"P{gamePlayers[i].PlayerNumber + 1}  ";
                }
            }
            return playerScores;
        }
        else
        {
            string playerScores = "Player cards value:\n";
            for (int i = 0; i < gamePlayers.Count; i++)
            {
                playerScores += $"P{gamePlayers[i].PlayerNumber + 1} = {gamePlayers[i].cardsValue}  ";
            }
            return playerScores;
        }
    }

    public void PlayerKilled()
    {
        victories++;
        ui.UpdateWinLose(victories, defeats);
    }

    //public void DealerBust()
    //{
    //    defeats += alivePlayers;
    //    ui.UpdateWinLose(victories, defeats);
    //}

    public void EndGame()
    {
        gameState = GameState.endGame;
        gameStateChanged?.Invoke(GameState.endGame);

        if (gamePlayers.Count > 0)
        {
            winLose = new bool[gamePlayers.Count];
        }
        for (int i = 0; i < gamePlayers.Count; i++)
        {
            if (deckManager.yourCardsValue > 21)
            {
                winLose[i] = false;
            }
            else
            {
                if (deckManager.yourCardsValue >= gamePlayers[i].cardsValue)
                {
                    winLose[i] = true;
                }
                else
                {
                    winLose[i] = false;
                }
            }
        }

        ui.UpdatePDealerUI(PlayersScoresText(true));
        ui.UpdateWinLose(victories, defeats);
    }

    public async void NewRound()
    {
        deckManager.RetrieveCards();

        await Task.Delay(500);

        foreach (var card in deckManager.deck)
        {
            card.cardTakenBack = true;
            card.isFaceUp = false;
        }

        foreach (var player in gamePlayers)
        {
            player.playerCards.Clear();
        }

        SetInitialGameState();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}

public enum GameState { waiting, firstTurn, dealerTurn, playersTurn, endGame }