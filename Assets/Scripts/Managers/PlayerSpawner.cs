using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    public GameObject table;
    public List<Vector3> playerPositions = new List<Vector3>();
    public GameObject playerExample;
    public GameObject playerSEELE;
    int lateralFreeSpace = 5;
    public List<int> spawnOrder = new List<int>();

    float semicircle;

    //private void Awake()
    //{
    //    SpawnPlayers(GameManager.Instance.Settings.playersNumber);
    //}

    public void SpawnPlayers(int playerNumber)
    {
        int positionIndex;

        if (playerNumber % 2 == 0)
        {
            positionIndex = 1;
        }
        else
        {
            positionIndex = 0;
        }

        PlayerAI[] players = new PlayerAI[playerNumber];

        for (int i = 0; i < playerNumber; i++)
        {
            var newPlayerObject = Instantiate(playerSEELE, playerPositions[spawnOrder[positionIndex + i]], Quaternion.identity);
            newPlayerObject.transform.LookAt(Vector3.zero);
            var newPlayerAI = newPlayerObject.GetComponent<PlayerAI>();

            int playerIndex;
            if (positionIndex == 0)
            {
                playerIndex = playerNumber - spawnOrder[positionIndex + i] - 1 + ((GameSettings.maxPayerNumber - playerNumber) / 2);
            }
            else
            {
                playerIndex = playerNumber - spawnOrder[positionIndex + i] - 1 + ((GameSettings.maxPayerNumber - ((positionIndex + i) % 2 - 1) - playerNumber) / 2);
            }

            players[playerIndex] = newPlayerAI;
            newPlayerAI.playerName.text = $"Player{playerIndex + 1}";
        }

        GameManager.Instance.gamePlayers = players.ToList();
    }

    public void ComputePlayerPositions()
    {
        playerPositions.Clear();
        spawnOrder.Clear();
        float degreesBetweenPlayers = (180 - (lateralFreeSpace * 2)) / (GameSettings.maxPayerNumber + 1);
        float playerDegreePosition = lateralFreeSpace + degreesBetweenPlayers;
        for (int i = 0; i < GameSettings.maxPayerNumber; i++)
        {
            float playerPositionX = table.transform.position.x + ((table.transform.localScale.x / 2) * Mathf.Cos((float)Utilities.DegreesToRadians((double)playerDegreePosition)));
            float playerPositionZ = table.transform.position.z + ((table.transform.localScale.z / 2) * Mathf.Sin((float)Utilities.DegreesToRadians((double)playerDegreePosition)));

            playerPositions.Add(new Vector3(playerPositionX, 0, playerPositionZ));

            playerDegreePosition += degreesBetweenPlayers;

        }

        OrderIndexes();
    }

    void OrderIndexes()
    {
        for (int i = 0; i < GameSettings.maxPayerNumber; i++)
        {
            spawnOrder.Add(i);
        }

        int start;

        if (GameSettings.maxPayerNumber % 2 == 0)
        {
            start = GameSettings.maxPayerNumber / 2;
        }
        else
        {
            start = (GameSettings.maxPayerNumber - 1) / 2;
        }

        int index = 0;
        int times = start;

        for (int i = 0; i < times; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                spawnOrder.Insert(index, spawnOrder[start]);
                spawnOrder.RemoveAt(start + 1);
                index++;
            }

            start++;
        }

    }

}
