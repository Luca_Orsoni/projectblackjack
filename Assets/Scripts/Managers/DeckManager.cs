using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.U2D;

public class DeckManager : MonoBehaviour
{
    public SpriteAtlas cardsAtlas;
    public GameObject cardPrefab;
    public GameObject distributor;
    public Transform dealerDeckPosition;
    public Transform CardsDisplayPoint;
    public Transform dealerHand;
    public List<CardObject> dealerCards = new List<CardObject>();
    public int yourCardsValue = 0;
    [SerializeField]
    public CardDescription[] cards = new CardDescription[52];
    public List<CardObject> deck = new List<CardObject>();
    public int cardsInDistributor;
    CancellationTokenSource source = new CancellationTokenSource();
    Vector3 cardOffset = new Vector3(0.015f, 0.002f, 0);
    public AudioSource deckAudio;

    public void GenerateCards()
    {
        cards = new CardDescription[52];
        Sprite[] cardsSprites = new Sprite[52];
        cardsAtlas.GetSprites(cardsSprites);

        List<CardDescription> spriteSorter = new List<CardDescription>();
        for (int i = 0; i < cardsSprites.Length; i++)
        {
            char[] tempName = cardsSprites[i].name.ToCharArray(6,2);
            if (tempName[1].Equals('('))
            {
                tempName[1] = tempName[0];
                tempName[0] = '0';

            }
            string newName = new string(tempName);
            cardsSprites[i].name = newName;
            spriteSorter.Add(new CardDescription(cardsSprites[i]));
        }
        spriteSorter.Sort();

        for (int i = 0; i < 52; i++)
        {
            int index = (i + 1) % 13;
            int v = index == 0 ? 10 : index > 10 ? 10 : index;
            cards[i] = new CardDescription(spriteSorter[i].front, v);
        }
    }

    public void CreateDeck()
    {
        deck.Clear();

        for (int i = 0; i < cards.Length; i++)
        {
            var newCard = Instantiate(cardPrefab, dealerDeckPosition.transform.position, Quaternion.identity);
            var cardComponent = newCard.GetComponent<CardObject>();

            cardComponent.SetCardDescription(cards[i]);

            newCard.SetActive(true);
            deck.Add(cardComponent);
            //deckCards[i] = cardComponent;
        }

    }

    public void DisplayCards()
    {
        float spaceBetweenCardsX = cardPrefab.transform.localScale.x;
        float spaceBetweenCardsZ = cardPrefab.transform.localScale.z;
        float posY = 0.3f;

        Vector3 cardPos = new Vector3((-spaceBetweenCardsX * 6) - 0.06f, posY, (spaceBetweenCardsZ * 2) + 0.1f);

        for (int i = 0; i < deck.Count; i++)
        {
            deck[i].RotateCard(CardRotation.faceUp);

            deck[i].gameObject.transform.SetParent(CardsDisplayPoint);
            deck[i].gameObject.transform.position = cardPos;

            int val = (i + 1) % 13;

            if (val == 0)
            {
                posY -= 0.02f;
                cardPos = new Vector3((-spaceBetweenCardsX * 6) - 0.06f, posY, cardPos.z - (spaceBetweenCardsZ + 0.02f));
            }
            else if (val < 13)
            {
                cardPos += new Vector3(spaceBetweenCardsX + 0.01f, 0, 0);
            }
        }
    }

    public void PlaceDeck()
    {
        if (deck.Count > 0)
        {
            float cardThickness = cardPrefab.transform.localScale.y / 4;
            float offset = 0.01f;
            for (int i = deck.Count - 1; i >= 0; i--)
            {
                deck[i].RotateCard(CardRotation.faceDown);
                deck[i].gameObject.transform.SetParent(dealerDeckPosition);
                deck[i].gameObject.transform.position = dealerDeckPosition.transform.localPosition + new Vector3(0, offset, 0);
                offset += cardThickness;
            }
        }
    }

    public void ShuffleDeck()
    {
        GameManager.Instance.UI.MessagesDisplayer.DismissMessage();
        deckAudio.PlayOneShot(GameManager.Instance.AudioManager.sounds[(int)Sounds.bridging]);
        List<CardDescription> shuffleList = cards.ToList();
        cards = new CardDescription[52];
        for (int i = 0; i < cards.Length; i++)
        {
            int j = Random.Range(0, shuffleList.Count);
            cards[i] = shuffleList[j];
            shuffleList.RemoveAt(j);
        }
        if (deck.Count > 0)
        {
            for (int i = 0; i < deck.Count; i++)
            {
                deck[i].SetCardDescription(cards[i], true);
            }
        }
        GameManager.Instance.deckMustBeShuffled = false;
        PlaceDeck();
    }

    public void GiveCard(PlayerAI player, bool isFirstTurn = false)
    {
        if (deck.Count > 0)
        {
            deck[0].gameObject.transform.SetParent(distributor.transform);
            cardsInDistributor++;
            deck[0].gameObject.transform.localEulerAngles = Vector3.zero;
            distributor.transform.LookAt(player.gameObject.transform);

            if (GameManager.Instance.gameState == GameState.firstTurn)
            {
                GameManager.Instance.UI.giveACardButton.SetActive(false);
            }
            deck[0].gameObject.transform.position += Vector3.up * 0.1f;

            if (player.waitingForACard)
            {
                player.GetCard(deck[0]);
            }
            else
            {
                deck[0].PlayGiveCard();
            }

            deck.RemoveAt(0);
        }
        else
        {
            Debug.LogError("Oops! There're no other cards �\\_(^^)_/�");
        }
    }

    public async void DrawACard()
    {
        bool arrived = false;
        source = new CancellationTokenSource();
        CancellationToken cancToken = source.Token;

        if (deck.Count > 0)
        {
            var newCard = deck[0];
            newCard.gameObject.transform.SetParent(dealerHand);
            dealerCards.Add(newCard);
            deck.RemoveAt(0);
            deckAudio.PlayOneShot(GameManager.Instance.AudioManager.sounds[(int)Sounds.dealing]);

            newCard.gameObject.transform.position += Vector3.up * 0.1f;
            while (!arrived)
            {
                Utilities.MoveObject(newCard.gameObject, dealerHand.position, 0.5f, Vector3.zero, 0.01f, out arrived);
                if (!cancToken.IsCancellationRequested)
                {
                    await Task.Yield();
                }
            }
        }
    }

    public async void FlipACard(int index)
    {
        bool arrived = false;
        source = new CancellationTokenSource();
        CancellationToken cancToken = source.Token;

        while (!arrived)
        {
            Utilities.MoveObject(dealerCards[index].gameObject, dealerHand.position, 0.5f, Vector3.zero, 0.01f, out arrived);
            if (!cancToken.IsCancellationRequested)
            {
                await Task.Yield();
            }
        }

        dealerCards[index].PlayFlipCard();
        dealerCards[index].isFaceUp = true;

        int newCardValue = dealerCards[index].Value;
        if (dealerCards[index].Value == 1)
        {
            newCardValue = Utilities.ChooseAceValue(yourCardsValue);
        }
        yourCardsValue += newCardValue;

        if (yourCardsValue > 21)
        {
            GameManager.Instance.UI.MessagesDisplayer.DisplayMessage("YOU BUST!");
            GameManager.Instance.UI.UpdateDDealerUI($"BUST");
            GameManager.Instance.EndGame();
        }
        else if (yourCardsValue == 21 && dealerCards.Count == 2)
        {
            GameManager.Instance.UI.MessagesDisplayer.DisplayMessage("NATURAL!!!");
            GameManager.Instance.UI.UpdateDDealerUI($"Your cards value: {yourCardsValue}");
            GameManager.Instance.EndGame();
        }
        else
        {
            GameManager.Instance.UI.UpdateDDealerUI($"Your cards value: {yourCardsValue}");
        }

        arrived = false;
        while (!arrived)
        {
            Utilities.MoveObject(dealerCards[index].gameObject, dealerHand.position - (Vector3.right * 0.4f) + (cardOffset * index), 0.5f, Vector3.zero, 0.01f, out arrived);
            if (!cancToken.IsCancellationRequested)
            {
                await Task.Yield();
            }
        }
    }

    public void FlipOrDrawACard()
    {
        int index;
        if (VerifyCardToFlip(out index))
        {
            FlipACard(index);
        }
        else
        {
            DrawACardFlipped();
        }
    }

    public void DrawACardFlipped()
    {
        DrawACard();
        FlipACard(dealerCards.Count - 1);
    }

    bool VerifyCardToFlip(out int index)
    {
        if (dealerCards.Count > 0)
        {
            if (!dealerCards[1].isFaceUp)
            {
                if (!dealerCards[0].isFaceUp)
                {
                    index = 0;
                }
                else
                {
                    index = 1;
                    GameManager.Instance.UI.FlipOrDrawCardsButtonUpdate(false);
                }

                return true;
            }
        }
        index = -1; //error
        return false;
    }

    public void RetrieveCards()
    {
        RetrieveAllCards();

        //deckAudio.PlayOneShot(GameManager.Instance.AudioManager.sounds[(int)Sounds.shuffling]);
        //for (int i = 0; i < GameManager.Instance.gamePlayers.Count; i++)
        //{
        //    for (int j = 0; j < GameManager.Instance.gamePlayers[i].playerCards.Count; j++)
        //    {
        //        RetrieveACard(GameManager.Instance.gamePlayers[i].playerCards[i]);
        //        GameManager.Instance.gamePlayers[i].playerCards.RemoveAt(i);
        //    }
        //}
        //for (int i = 0; i < dealerCards.Count; i++)
        //{
        //    RetrieveACard(dealerCards[i]);
        //    dealerCards.RemoveAt(i);
        //}
        //RetrieveOverdistributedCards();
    }

    void RetrieveAllCards()
    {
        CardObject[] cardsToRetrieve = FindObjectsOfType<CardObject>();
        foreach (var card in cardsToRetrieve)
        {
            if (!deck.Contains(card))
            {
                RetrieveACard(card);
            }
        }

        dealerCards.Clear();
        cardsInDistributor = 0;

    }

    public void RetrieveOverdistributedCards()
    {
        foreach (var card in distributor.GetComponentsInChildren<CardObject>())
        {
            RetrieveACard(card);
            cardsInDistributor--;
        }
    }

    public void RetrieveACard(CardObject card)
    {
        card.cardTakenBack = false;
        card.RotateCard(CardRotation.faceDown);
        card.gameObject.transform.SetParent(dealerDeckPosition);
        card.gameObject.transform.eulerAngles = Vector3.zero;
        deck.Add(card);
        PlaceDeck();
    }

    public void GiveCardButton()
    {
        GameManager.Instance.UI.MessagesDisplayer.DismissMessage();
        if (GameManager.Instance.activePlayer != GameManager.Instance.alivePlayers && GameManager.Instance.activePlayer >= 0)
        {
            GiveCard(GameManager.Instance.gamePlayers[GameManager.Instance.activePlayer]);
        }
    }

    private void OnDisable()
    {
        if (source != null)
        {
            source.Cancel();
            source = null;
        }

        deck.Clear();
    }
}

