using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSettings : MonoBehaviour
{
    [SerializeField]
    private AudioManager audioManager;

    public Slider sliderMaster;
    [HideInInspector]
    public float sliderValueMaster;

    public Slider sliderMusic;
    [HideInInspector]
    public float sliderValueMusic;

    public Slider sliderSFX;
    [HideInInspector]
    public float sliderValueSFX;


    public void SetLevelMaster()
    {
        sliderValueMaster = sliderMaster.value;
        audioManager.mixers[(int)Mixers.Master].audioMixer.SetFloat("MasterVolume", Mathf.Log10(sliderValueMaster) * 20);
        PlayerPrefs.SetFloat("MasterVolume", sliderValueMaster);
    }

    public void SetLevelMusic()
    {
        sliderValueMusic = sliderMusic.value;
        audioManager.mixers[(int)Mixers.Music].audioMixer.SetFloat("MusicVolume", Mathf.Log10(sliderValueMusic) * 20);
        PlayerPrefs.SetFloat("MusicVolume", sliderValueMusic);
    }

    public void SetLevelSFX()
    {
        sliderValueSFX = sliderSFX.value;
        audioManager.mixers[(int)Mixers.SFX].audioMixer.SetFloat("SFXVolume", Mathf.Log10(sliderValueSFX) * 20);
        PlayerPrefs.SetFloat("SFXVolume", sliderValueSFX);
    }

    void Awake()
    {
        LoadAudioSettings();
    }

    public void LoadAudioSettings()
    {
        sliderMaster.value = PlayerPrefs.GetFloat("MasterVolume", 0.75f);
        sliderValueMaster = sliderMaster.value;
        audioManager.mixers[(int)Mixers.Master].audioMixer.SetFloat("MasterVolume", Mathf.Log10(sliderValueMaster) * 20);

        sliderMusic.value = PlayerPrefs.GetFloat("MusicVolume", 0.75f);
        sliderValueMusic = sliderMusic.value;
        audioManager.mixers[(int)Mixers.Music].audioMixer.SetFloat("MusicVolume", Mathf.Log10(sliderValueMusic) * 20);

        sliderSFX.value = PlayerPrefs.GetFloat("SFXVolume", 0.75f);
        sliderValueSFX = sliderSFX.value;
        audioManager.mixers[(int)Mixers.SFX].audioMixer.SetFloat("SFXVolume", Mathf.Log10(sliderValueSFX) * 20);
    }
}
