using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public List<AudioMixerGroup> mixers;
    public List<AudioClip> sounds = new List<AudioClip>();
}
public enum Mixers { Master, Music, SFX }
public enum Sounds { flipCard, flipCard2, flipCard3, shuffling, dealing, bridging }