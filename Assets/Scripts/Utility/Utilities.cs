using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities
{
    public static double DegreesToRadians(double angle)
    {
        return (Math.PI / 180) * angle;
    }

    public static void MoveObject(GameObject objectToMove, Vector3 destination, float speed, Vector3 offset, float acceptableDistance, out bool arrived)
    {
        var pos = Vector3.MoveTowards(objectToMove.gameObject.transform.position, destination + offset, Time.deltaTime * speed);
        objectToMove.gameObject.transform.position = pos;
        if (Vector3.Distance(objectToMove.gameObject.transform.position, destination) <= (Mathf.Abs(acceptableDistance)))
        {
            arrived = true;
        }
        else
        {
            arrived = false;
        }
    }

    public static int ChooseAceValue(int cardsValue)
    {
        if (cardsValue < 11)
        {
            return 11;
        }

        return 1;
    }

}
