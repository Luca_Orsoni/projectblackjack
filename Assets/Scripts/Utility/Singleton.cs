using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
	private static T instance;

    public bool destroyedOnLoad;

	public virtual void Awake()
	{
        if(Instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = (T)this;
            if (!destroyedOnLoad)
            {
                DontDestroyOnLoad(gameObject);
            }
        }
    }

    public static T Instance
    {
        get
        {
            return instance;
        }
    }
}
