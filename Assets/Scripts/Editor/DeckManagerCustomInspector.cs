using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DeckManager))]
public class DeckManagerCustomInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DeckManager dm = (DeckManager)target;

        if (GUILayout.Button("Create cards sprites list"))
        {
            dm.GenerateCards();
        }

        if (GUILayout.Button("Create deck"))
        {
            dm.CreateDeck();
        }
    }
}