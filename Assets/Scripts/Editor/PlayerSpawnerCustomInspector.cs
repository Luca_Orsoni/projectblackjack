using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerSpawner))]
public class PlayerSpawnerCustomInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PlayerSpawner ps = (PlayerSpawner)target;

        if (GUILayout.Button("Create palyers spawn points"))
        {
            ps.ComputePlayerPositions();
        }
    }
}
