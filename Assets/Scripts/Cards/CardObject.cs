using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class CardObject : MonoBehaviour
{
    [SerializeField]
    protected SpriteRenderer frontImage;
    [SerializeField]
    protected AudioSource audioSource;
    [SerializeField]
    CardDescription card;
    public bool isFaceUp = true;
    Animation anim;
    AnimationClip giveCard;
    AnimationClip drawCard;
    AnimationClip FlipCard;
    public bool cardTakenBack = false;

    #region Properties

    public Sprite Front
    {
        get
        {
            return card.front;
        }
        set
        {
            card.front = value;
        }
    }
    public int Value
    {
        get
        {
            return card.cardValue;
        }
        set
        {
            card.cardValue = value;
        }
    }

    #endregion

    private void Start()
    {
        SetCardImage();
        anim = GetComponent<Animation>();
        giveCard = anim.GetClip("GiveCard");
        drawCard = anim.GetClip("DrawCard");
        FlipCard = anim.GetClip("FlipCard");
    }
    private void Update()
    {
        if (!cardTakenBack)
        {
            Utilities.MoveObject(gameObject, GameManager.Instance.DeckManager.dealerDeckPosition.position, 0.5f, Vector3.zero, 0.01f, out cardTakenBack);
        }
    }

    void SetCardImage()
    {
        frontImage.sprite = Front;
    }

    public void SetCardDescription(CardDescription newDescription, bool setAlsoImage = false)
    {
        card = newDescription;

        if (setAlsoImage)
        {
            SetCardImage();
        }
    }

    public void RotateCard()
    {
        if (isFaceUp)
        {
            gameObject.transform.eulerAngles = new Vector3(0, 0, 180);
            isFaceUp = false;
        }
        else
        {
            gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            isFaceUp = true;
        }
        transform.parent.Rotate(new Vector3(0, 0, 180));
    }

    public void RotateCard(CardRotation newRotation)
    {
        switch (newRotation)
        {
            case CardRotation.faceUp:
                gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
                isFaceUp = true;
                break;
            case CardRotation.faceDown:
                gameObject.transform.eulerAngles = new Vector3(0, 0, 180);
                isFaceUp = false;
                break;
            default:
                break;
        }
    }

    public Task PlayGiveCardAnimation()
    {
        //giveCard.Play();
        //return Task.Delay((int)(giveCard.GetClip("GiveCard").length * 1000));

        return Task.Delay(0);
    }

    public void PlayGiveCard()
    {
        anim.clip = giveCard;
        anim.Play();
        audioSource.PlayOneShot(GameManager.Instance.AudioManager.sounds[(int)Sounds.flipCard2]);
    }

    public void PlayDrawCard()
    {
        anim.clip = drawCard;
        anim.Play();
        audioSource.PlayOneShot(GameManager.Instance.AudioManager.sounds[(int)Sounds.flipCard3]);
    }
    public void PlayFlipCard()
    {
        anim.clip = FlipCard;
        anim.Play();
        audioSource.PlayOneShot(GameManager.Instance.AudioManager.sounds[(int)Sounds.flipCard]);
    }
}

public enum CardRotation { faceUp, faceDown }
