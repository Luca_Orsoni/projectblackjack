using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class CardDescription : IComparable<CardDescription>
{
    public Sprite front;
    public int cardValue;

    public CardDescription(Sprite cardSprite, int value = 0)
    {
        front = cardSprite;
        cardValue = value;
    }

    public int CompareTo(CardDescription other)
    {
        if (int.TryParse(front.name, out int a) && int.TryParse(other.front.name, out int b))
        {
            if (a < b) return -1;
            else if (a > b) return 1;
            else return 0;
        }
        else
        {
            return 0;
        }
    }
}
