using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    public static int maxPayerNumber = 7;
    [Range(1, 7)]
    public int playersNumber = 1;
    [Range(1, 3)]
    public int aiLevel = 2;

    public Slider playersSlider;
    [HideInInspector]
    public float playerValue;

    public Slider aiSlider;
    [HideInInspector]
    public float levelValue;

    public void SetPlayerNumber()
    {
        playerValue = playersSlider.value;
        playersNumber = (int)playerValue;
        PlayerPrefs.SetInt("PlayersNumber", playersNumber);
    }
    public void SetAILevel()
    {
        levelValue = aiSlider.value;
        aiLevel = (int)levelValue;
        PlayerPrefs.SetInt("AILevel", aiLevel);
    }

    void Awake()
    {
        LoadGameSettings();
    }

    public void LoadGameSettings()
    {
        playersSlider.value = PlayerPrefs.GetInt("PlayersNumber", 7);
        playerValue = playersSlider.value;
        playersNumber = (int)playerValue;

        aiSlider.value = PlayerPrefs.GetInt("AILevel", 2);
        levelValue = aiSlider.value;
        aiLevel = (int)levelValue;
    }
}
