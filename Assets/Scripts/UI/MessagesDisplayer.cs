using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class MessagesDisplayer : MonoBehaviour
{
    public TMP_Text messages;
    public Color startColor;
    CancellationTokenSource source = new CancellationTokenSource();

    private void Start()
    {
        startColor = messages.color;
    }

    public void DisplayMessagesButton(string message)
    {
        DisplayMessage(message);
    }

    public void DisplayMessage(string message = "Message not set", float timer = 5, float delay = 3)
    {
        source.Cancel();
        source = null;
        source = new CancellationTokenSource();

        messages.alpha = 1;
        messages.text = message;

        if (timer > 0)
        {
            Fade(timer, delay, source.Token);
        }

    }

    async void Fade(float timer, float delay, CancellationToken cancToken)
    {
        Color fadeColor = messages.color;
        fadeColor.a = 0;

        for (float t = 0; t < delay; t += Time.deltaTime)
        {
            if (!cancToken.IsCancellationRequested)
            {
                await Task.Yield();
            }
        }       

        for (float t = 0; t < timer; t += Time.deltaTime)
        {
            if (!cancToken.IsCancellationRequested)
            {
                messages.alpha = Mathf.LerpUnclamped(startColor.a, fadeColor.a, t);
                await Task.Yield();
            }
        }

    }

    public void DismissMessage()
    {
        messages.alpha = 0;
    }

    protected void OnDisable()
    {
        if (source != null)
        {
            source.Cancel();
            source = null;
        }
    }

}
