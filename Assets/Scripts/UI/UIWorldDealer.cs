using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIWorldDealer : MonoBehaviour
{
    public GameObject nextPlayerButton;
    public GameObject drawACardButton;
    public GameObject endRoundButton;
    public TMP_Text playersScores;
    public TMP_Text dealerScore;
}
