using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    [HideInInspector]
    public string sceneToLoad;
    public GameObject mainMenuButton;
    public GameObject settingsButton;

    private void Start()
    {
        if (SceneManager.GetActiveScene().name.Contains("Game"))
        {
            settingsButton.SetActive(false);
        }
        else if (SceneManager.GetActiveScene().name.Contains("Menu"))
        {
            mainMenuButton.SetActive(false);
        }
    }

    public void LoadNewScene(string insertedSceneToLoad)
    {
        SceneManager.LoadScene(insertedSceneToLoad);
    
    }

    public void ToMain()
    {
        LoadNewScene("MainMenuScene");
    }

    public void Quit()
    {
        Application.Quit();
    }

}
