using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private MessagesDisplayer messagesDisplayer;
    public GameObject startPlayButton;
    public GameObject worldUIPlayer;
    public UIWorldDealer uiWorldDealer;
    public GameObject giveACardButton;
    public GameObject killPlayer;
    public GameObject displayCardsButton;
    public GameObject shuffleButton;
    public GameObject placeDeckButton;
    public GameObject nextButton;
    public GameObject retrieveCardsButton;
    public GameObject newRoundButton;
    public TMP_Text victories;
    public TMP_Text defeats;

    public MessagesDisplayer MessagesDisplayer
    {
        get
        {
            return messagesDisplayer;
        }
    }

    public void StartPlay()
    {
        if (GameManager.Instance.deckMustBeShuffled)
        {
            messagesDisplayer.DisplayMessage("You have to shuffle the deck first", 0, 0);
        }
        else
        {
            GameManager.gameStateChanged += OnGameStateChange;
            GameManager.Instance.StartFirstTurn();
            if (!GameManager.Instance.deckMustBeShuffled)
            {
                startPlayButton.SetActive(false);
            }
        }
    }

    public void UpdateWinLose(int win, int loss)
    {
        victories.text = win.ToString();
        defeats.text = loss.ToString();
    }

    void OnGameStateChange(GameState newGameState)
    {
        switch (newGameState)
        {
            case GameState.waiting:
                SetWaitingUI();
                break;
            case GameState.firstTurn:
                SetFirstTurnUI();
                break;
            case GameState.dealerTurn:
                SetDealerTurnUI();
                break;
            case GameState.playersTurn:
                SetPlayerTurnUI();
                break;
            case GameState.endGame:
                SetEndGameUI();
                break;
            default:
                break;
        }
    }

    void SetWaitingUI()
    {
        //nextButton.SetActive(true);
        newRoundButton.SetActive(false);
        retrieveCardsButton.SetActive(false);
        startPlayButton.SetActive(true);
        giveACardButton.SetActive(false);
    }
    void SetFirstTurnUI()
    {
        startPlayButton.SetActive(false);
        displayCardsButton.SetActive(false);
        shuffleButton.SetActive(false);
        placeDeckButton.SetActive(false);
    }
    void SetDealerTurnUI()
    {
        uiWorldDealer.drawACardButton.gameObject.SetActive(true);
        uiWorldDealer.endRoundButton.gameObject.SetActive(true);
        nextButton.SetActive(false);
        giveACardButton.SetActive(false);
        uiWorldDealer.gameObject.SetActive(true);
    }
    void SetPlayerTurnUI()
    {
        if (GameManager.Instance.gamePlayers[GameManager.Instance.activePlayer].busted)
        {
            killPlayer.SetActive(true);
        }
        else
        {
            killPlayer.SetActive(false);
        }
        nextButton.SetActive(true);
        retrieveCardsButton.SetActive(true);
        giveACardButton.SetActive(true);
        uiWorldDealer.gameObject.SetActive(false);
    }
    void SetEndGameUI()
    {
        newRoundButton.SetActive(true);
        retrieveCardsButton.SetActive(true);
        uiWorldDealer.drawACardButton.gameObject.SetActive(false);
        uiWorldDealer.endRoundButton.gameObject.SetActive(false);
    }

    public void Next()
    {
        bool avoid = false;
        if (GameManager.Instance.activePlayer != GameManager.Instance.alivePlayers && GameManager.Instance.activePlayer >= 0)
        {
            if (GameManager.Instance.gamePlayers[GameManager.Instance.activePlayer].waitingForACard)
            {
                avoid = true;
                messagesDisplayer.DisplayMessage("It's asking for a card", 0, 0);
            }
        }
        if (!avoid)
        {
            //if (GameManager.Instance.DeckManager.cardsInDistributor > 0)
            //{
            //    GameManager.Instance.deckMustBeShuffled = true;
            //}
            GameManager.Instance.CameraManager.StopRotation();
            int pn = GameManager.Instance.alivePlayers;
            switch (GameManager.Instance.activePlayer)
            {
                case int n when (n + 2 == pn):
                    if (GameManager.Instance.activePlayer >= 0)
                    {
                        GameManager.Instance.gamePlayers[GameManager.Instance.activePlayer].playerScore.transform.parent.gameObject.SetActive(false);
                    }
                    uiWorldDealer.nextPlayerButton.GetComponentInChildren<TMP_Text>().text = "Yout turn";
                    break;
                case int n when (n == pn):
                    uiWorldDealer.nextPlayerButton.GetComponentInChildren<TMP_Text>().text = "Next player";
                    break;
                case int n when (n + 1 == pn):
                    if (GameManager.Instance.activePlayer >= 0)
                    {
                        GameManager.Instance.gamePlayers[GameManager.Instance.activePlayer].playerScore.transform.parent.gameObject.SetActive(false);
                    }
                    uiWorldDealer.nextPlayerButton.GetComponentInChildren<TMP_Text>().text = "Players turn";
                    break;
                default:
                    if (GameManager.Instance.activePlayer >= 0)
                    {
                        GameManager.Instance.gamePlayers[GameManager.Instance.activePlayer].playerScore.transform.parent.gameObject.SetActive(false);
                    }
                    break;
            }
            GameManager.Instance.SetActivePlayer();
        }
    }

    public void DeactivateAll()
    {
        nextButton.SetActive(false);
        newRoundButton.SetActive(false);
        retrieveCardsButton.SetActive(false);
        startPlayButton.SetActive(false);
        giveACardButton.SetActive(false);
        displayCardsButton.SetActive(false);
        shuffleButton.SetActive(false);
        placeDeckButton.SetActive(false);
        uiWorldDealer.drawACardButton.gameObject.SetActive(false);
        uiWorldDealer.endRoundButton.gameObject.SetActive(false);
    }

    public void KillPlayerButton()
    {
        GameManager.Instance.gamePlayers[GameManager.Instance.activePlayer].KillPlayer();
        GameManager.Instance.PlayerKilled();
        killPlayer.SetActive(false);
    }

    public void UpdateActivePlayerScore(int playerNumber, int newScore)
    {
        GameManager.Instance.gamePlayers[playerNumber].playerScore.text = $"Actual score: {newScore}";
    }

    public void UpdatePDealerUI(string pScores)
    {
        uiWorldDealer.playersScores.text = pScores;
    }
    public void UpdateDDealerUI(string dScores)
    {
        uiWorldDealer.dealerScore.text = dScores;
    }

    public void FlipOrDrawCardsButtonUpdate(bool flip)
    {
        if (flip)
        {
            uiWorldDealer.drawACardButton.GetComponentInChildren<TMP_Text>().text = "Flip Cards";
        }
        else
        {
            uiWorldDealer.drawACardButton.GetComponentInChildren<TMP_Text>().text = "Draw a card";
        }
    }

    public void ShowChoiceInUI(PlayerCall choice)
    {
        switch (choice)
        {
            case PlayerCall.thinking:
                break;
            case PlayerCall.card:
                messagesDisplayer.DisplayMessage("Card", 0, 0);
                break;
            case PlayerCall.stop:
                messagesDisplayer.DisplayMessage("Stop", 0, 0);
                break;
            case PlayerCall.bust:
                messagesDisplayer.DisplayMessage("Bust", 0, 0);
                killPlayer.SetActive(true);
                break;
            case PlayerCall.natural:
                messagesDisplayer.DisplayMessage("NATURAL!", 0, 0);
                break;
            default:
                break;
        }
    }

    private void OnDisable()
    {
        GameManager.gameStateChanged -= OnGameStateChange;
    }
}
