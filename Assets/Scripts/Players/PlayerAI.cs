using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class PlayerAI : MonoBehaviour
{
    int riskFactor = 0;
    int randomRiskFactorAcceptance;
    public int maxRiskAcceptance = 3;
    public List<CardObject> playerCards = new List<CardObject>();
    public int cardsValue;
    public PlayerCall choice;
    [SerializeField]
    protected Transform playerHand;
    bool cardAlreadyTaken = true;
    bool playerBuried = true;
    public bool waitingForACard = true;
    public float speed = 0.1f;
    public Vector3 cardOffset = new Vector3(0.015f, 0.005f, 0);
    public TMP_Text playerScore;
    public TMP_Text playerName;
    public GameObject x;

    public bool busted
    {
        get
        {
            if (cardsValue > 21)
            {
                return true;
            }
            return false;
        }
    }

    bool natural
    {
        get
        {
            if (cardsValue == 21 && playerCards.Count == 2)
            {
                return true;
            }
            return false;
        }
    }

    public int PlayerNumber
    {
        get
        {
            int result;
            if (int.TryParse(playerName.text.Substring(playerName.text.Length - 1), out result))
            {
                return result - 1;
            }
            else
            {
                return 0;
            }
        }
    }

    private void Start()
    {
        randomRiskFactorAcceptance = Random.Range(-3 , 4);
        maxRiskAcceptance += randomRiskFactorAcceptance + GameManager.Instance.Settings.aiLevel;
    }

    private void Update()
    {
        if (!cardAlreadyTaken)
        {
            Utilities.MoveObject(playerCards[playerCards.Count - 1].gameObject, playerHand.position, speed, cardOffset * (playerCards.Count - 1), 0.15f, out cardAlreadyTaken);
        }
        if (!playerBuried)
        {
            Utilities.MoveObject(gameObject, new Vector3(transform.position.x, -2f, transform.position.z), speed, Vector3.zero, 0.15f, out playerBuried);
        }
    }

    public void MakeChoice()
    {
        if (busted)
        {
            waitingForACard = false;
            choice = PlayerCall.bust;
        }
        else
        {
            if (natural)
            {
                waitingForACard = false;
                choice = PlayerCall.natural;
            }
            else
            {
                VerifyActualRiskLevel();

                float decisionValue = riskFactor + randomRiskFactorAcceptance;

                if (decisionValue < maxRiskAcceptance)
                {
                    waitingForACard = true;
                    choice = PlayerCall.card;
                }
                else
                {
                    waitingForACard = false;
                    choice = PlayerCall.stop;
                }
            }
        }
        Debug.Log($"Choosed {choice} with value {cardsValue} and random risk factor {randomRiskFactorAcceptance}");
    }

    public void ShowChoice()
    {
        GameManager.Instance.UI.ShowChoiceInUI(choice);
        if (choice == PlayerCall.bust)
        {
            playerScore.transform.parent.gameObject.SetActive(false);
        }
    }

    public void KillPlayer()
    {
        cardAlreadyTaken = true;
        x.SetActive(true);
        var mr = GetComponentInChildren<MeshRenderer>();
        mr.material.color = Color.red;
        foreach (var card in playerCards)
        {
            GameManager.Instance.DeckManager.RetrieveACard(card);
        }
        playerBuried = false;
        playerCards.Clear();
        GameManager.Instance.RemovePlayer(this);
    }

    PlayerCall VerifyHand()
    {
        return busted ? PlayerCall.bust : PlayerCall.thinking;
    }

    public async void GetCard(CardObject newCard)
    {

        if (waitingForACard)
        {
            choice = PlayerCall.thinking;

            waitingForACard = false;

            GameManager.Instance.DeckManager.cardsInDistributor--;

            playerCards.Add(newCard);

            newCard.gameObject.transform.SetParent(playerHand);
            newCard.gameObject.transform.localEulerAngles = Vector3.zero;

            cardAlreadyTaken = false;

            int valueToAdd;

            if (newCard.Value == 1)
            {
                valueToAdd = Utilities.ChooseAceValue(cardsValue);
            }
            else
            {
                valueToAdd = newCard.Value;
            }

            cardsValue += valueToAdd;

            playerScore.text = $"Actual score: {cardsValue}";

            Debug.Log($"Actual score: {cardsValue}");

            //choice = VerifyHand();

            await newCard.PlayGiveCardAnimation();

            if (GameManager.Instance.gameState != GameState.firstTurn && choice == PlayerCall.thinking)
            {
                GameManager.Instance.ContinuePlayerTurn();
            }
        }

    }

    void VerifyActualRiskLevel()
    {
        switch (cardsValue)
        {
            case int n when (n > 20):
                riskFactor = 9;
                break;
            case int n when (n > 15):
                riskFactor = 7;
                break;
            case int n when (n > 13):
                riskFactor = 5;
                break;
            case int n when (n > 11):
                riskFactor = 3;
                break;
            default:
                riskFactor = 0;
                break;
        }
        Debug.Log($"Actual Risk Level {riskFactor} with {cardsValue} and random risk factor {randomRiskFactorAcceptance}");
    }

    public void PlayTurn()
    {
        VerifyActualRiskLevel();
        Debug.Log($"ActualRiskLevel = {riskFactor}");
    }
}

public enum PlayerCall { thinking, card, stop, bust, natural}